#include "board.h"

namespace{
    struct QPointCompare{
        bool operator()(const QPoint& point1, const QPoint& point2){
            return (point1.x() < point2.x()) || (point1.y() < point2.y());
        }
    };
}

Board::Board(int rows, int cols) : startPoint(QPoint(5,0)), rows(rows), cols(cols), blockPtr(nullptr) {
    board.resize(rows*cols);
    board.fill(BlockType::None);
}
//QPoint(x,y) x = col number, y = row number
bool Board::addNewBlock(BlockType blockType){
    currentBlockType = blockType;
    blockPtr.reset(new Block(blockType, startPoint));
    if(detectBlockCollision(blockPtr->getPoints())){
        return false;
    }
    mapBlockToBoard(blockPtr->getPoints());
    return true;
}
void Board::mapBlockToBoard(const QVector<QPoint>& currentBlockCoords){
    for(auto point : currentBlockCoords){
        board[index(point.y(),point.x())] = currentBlockType;
    }
}
void Board::cleanBlocks(const QVector<QPoint>& coords){
    for(auto point : coords){
        board[index(point.y(),point.x())] = BlockType::None;
    }
}
int Board::index(int i, int j){
    return i*cols+j;
}
bool Board::detectBlockCollision(Direction direction){
    QPoint step;
    if(direction == Direction::Left){
        step = QPoint(-1,0);
    }else if(direction == Direction::Right){
        step = QPoint(1,0);
    }else if(direction == Direction::Down){
        step = QPoint(0,1);
    }else{
        return true;
    }
    QVector<QPoint> currentBlockCoords = blockPtr->getPoints();
    cleanBlocks(currentBlockCoords);
    for(auto point : currentBlockCoords){
        QPoint movedPoint = point+step;
        if(movedPoint.x()<0 || movedPoint.y()<0 || movedPoint.x()>=cols || movedPoint.y()>=rows){
            mapBlockToBoard(currentBlockCoords);
            return true;
        }
        if(board[index(movedPoint.y(),movedPoint.x())]!=BlockType::None){
            mapBlockToBoard(currentBlockCoords);
            return true;
        }
    }
    mapBlockToBoard(currentBlockCoords);
    return false;//no collision detected
}
bool Board::detectBlockCollision(const QVector<QPoint>& coords){
    for(auto point : coords){
        if(point.x()<0 || point.y()<0 || point.x()>=cols || point.y()>=rows || board[index(point.y(),point.x())]!=BlockType::None){
            return true;
        }
    }
    return false;//no collision detected
}
void Board::rotateBlock90(){
    QVector<QPoint> oldCoords = blockPtr->getPoints();
    blockPtr->rotate90();
    cleanBlocks(oldCoords);
    if(detectBlockCollision(blockPtr->getPoints()) || !isBlockPointsValid(blockPtr->getPoints())){
        blockPtr->changePoints(oldCoords);
    }
    mapBlockToBoard(blockPtr->getPoints());
}
bool Board::isBlockPointsValid(const QVector<QPoint> &currentBlockCoords){
    std::set<QPoint, QPointCompare> blockPointsSet;
    for(auto point : currentBlockCoords){
        blockPointsSet.insert(point);
    }
    bool result = (static_cast<int>(blockPointsSet.size()) == currentBlockCoords.size()) ? true : false;
    return result;
}
bool Board::checkIsRowFill(int rowNumber){
    for(int i=0;i<cols;i++){
        if(board[rowNumber*cols+i] == BlockType::None){
            return false;
        }
    }
    return true;
}
void Board::removeRows(QVector<int> rowsToRemove){//TODO improve that method
    QVector<BlockType> boardCopy(cols*rows,BlockType::None);
    for(int i=rowsToRemove.size(),x=0;i<rows;i++,x++){
        if(rowsToRemove.contains(x)) x++;
        for(int j=0;j<cols;j++){
            boardCopy[index(i,j)] = board[index(x,j)];
        }
    }
    board.swap(boardCopy);
}
int Board::checkBoard(){
    int score = 0;
    QVector<int> rowsToRemove;
    for(int r = (rows-1);r>0;r--){
        if(checkIsRowFill(r)){
            rowsToRemove.push_back(r);
            score+=1;
        }
    }
    removeRows(rowsToRemove);
    return score;
}
void Board::moveBlock(Direction direction){
    cleanBlocks(blockPtr->getPoints());
    blockPtr->move(direction);
    mapBlockToBoard(blockPtr->getPoints());
}
Board::~Board(){

}
