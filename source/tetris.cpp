#include "tetris.h"
#include <QTimer>

Tetris::Tetris(QWidget *parent)
    : QWidget(parent)
{
    repaintTimer = startTimer(20);
    moveDownTimer = startTimer(500);
    hitBottom = 0;
    score = 0;
    isGameOver = false;
    isPaused = false;
    board = new Board(board_height_in_blocks, board_width_in_blocks);
    board->addNewBlock(Block::getRandomBlock());
}

void Tetris::drawBoard(QPainter* painter){
    QPen pen(Qt::black, 2, Qt::SolidLine);
    painter->setPen(pen);
    painter->drawRect(board_start_x-2, board_start_y, board_width+3, board_height+1);
    painter->drawText(board_start_x + board_width + 20, board_start_y + 20, QString("Your score: ") + QString::number(score));
    int draw_block_start_x = board_start_x;
    int draw_block_start_y = board_start_y;
    auto tmpBoard = board->getBoard();
    for(int i=0;i<board_height_in_blocks;i++){
        for(int j=0;j<board_width_in_blocks;j++){
            BlockType actualBlockType = tmpBoard[i*board_width_in_blocks+j];
            if(actualBlockType != BlockType::None){
                QRect block(draw_block_start_x, draw_block_start_y, block_size-1, block_size-1);
                painter->drawRect(block);
                painter->fillRect(block, QBrush(Block::getBlockColor(actualBlockType)));
            }
            draw_block_start_x+=block_size;
        }
       draw_block_start_y+=block_size;
       draw_block_start_x=board_start_x;
    }
}

void Tetris::paintEvent(QPaintEvent* event){
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    if(!isGameOver){
        drawBoard(&painter);
    }else{
        QString gameOverMessage("Game over");
        QString scoreMessage = QString("Your score: ")+QString::number(score);
        QFont font("Roboto", 30);
        QFontMetrics fm(font);
        painter.setFont(font);
        painter.drawText(board_width/2, board_height/2, gameOverMessage);
        painter.drawText(board_width/2, board_height/2 + fm.height(), scoreMessage);
    }
}
void Tetris::keyPressEvent(QKeyEvent* event){
    if(isGameOver){
        return;
    }
    int key = event->key();
    if(key == Qt::Key_P){
        isPaused=!isPaused;
    }
    if(isPaused){
        return;
    }
    if(key == Qt::Key_Left){
        if(!board->detectBlockCollision(Direction::Left)){
            board->moveBlock(Direction::Left);
        }
    }else if(key == Qt::Key_Right){
        if(!board->detectBlockCollision(Direction::Right)){
            board->moveBlock(Direction::Right);
        }
    }else if(key == Qt::Key_Down){
        if(!board->detectBlockCollision(Direction::Down)){
            board->moveBlock(Direction::Down);
        }
    }else if(key == Qt::Key_Up){
        board->rotateBlock90();
    }else if(key == Qt::Key_Escape){
        close();
    }
}
void Tetris::timerEvent(QTimerEvent *event){
    if(event->timerId() == moveDownTimer && !isPaused){
        if(!board->detectBlockCollision(Direction::Down)){
            board->moveBlock(Direction::Down);
        }else{
            hitBottom++;
        }
        if(hitBottom == 2){
            int roundScore = board->checkBoard();
            score+=roundScore > 0 ? roundScore*10 + 10*(roundScore-1) : 0;
            isGameOver = !board->addNewBlock(Block::getRandomBlock());
            hitBottom = 0;
        }
    }
    repaint();
}
Tetris::~Tetris()
{
    delete board;
}

