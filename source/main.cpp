#include "tetris.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Tetris w;
    w.show();
    w.resize(600, 800);
    return a.exec();
}
