#include "block.h"

Block::Block(BlockType blockType,QPoint startPoint) {
    this->blockType = blockType;
    blockPoints.resize(4);
    blockPoints[0]=startPoint;
    if(blockType == BlockType::Long){
        for(int i=1;i<blockPoints.size();i++){
            blockPoints[i] = blockPoints[i-1]+QPoint(0,1);
        }
    }else if(blockType == BlockType::Square){
        blockPoints[1] = startPoint+QPoint(1,0);
        blockPoints[2] = startPoint+QPoint(0,1);
        blockPoints[3] = startPoint+QPoint(1,1);
    }else if(blockType == BlockType::Piramid){
        blockPoints[1] = startPoint+QPoint(0,1);
        blockPoints[2] = blockPoints[1]+QPoint(1,0);
        blockPoints[3] = blockPoints[1]+QPoint(0,1);
    }else if(blockType == BlockType::Lefty){
        blockPoints[1] = startPoint+QPoint(0,1);
        blockPoints[2] = blockPoints[1]+QPoint(1,0);
        blockPoints[3] = blockPoints[2]+QPoint(0,1);
    }else if(blockType == BlockType::Righty){
        blockPoints[1] = startPoint+QPoint(0,1);
        blockPoints[2] = blockPoints[1]+QPoint(-1,0);
        blockPoints[3] = blockPoints[2]+QPoint(0,1);
    }else if(blockType == BlockType::Ltype){
        blockPoints[1] = startPoint+QPoint(0,1);
        blockPoints[2] = blockPoints[1]+QPoint(0,1);
        blockPoints[3] = blockPoints[2] + QPoint(1, 0);
    }else if(blockType == BlockType::InvLtype){
        blockPoints[1] = startPoint+QPoint(0,1);
        blockPoints[2] = blockPoints[1]+QPoint(0,1);
        blockPoints[3] = blockPoints[2] + QPoint(-1, 0);
    }
}
const QVector<QPoint>& Block::getPoints(){
    return blockPoints;
}
void Block::move(Direction direction){
    QPoint step;
    if(direction == Direction::Left){
        step = QPoint(-1,0);
    }else if(direction == Direction::Right){
        step = QPoint(1, 0);
    }else if(direction == Direction::Down){
        step = QPoint(0, 1);
    }
    std::transform(blockPoints.begin(),blockPoints.end(),blockPoints.begin(),[step](QPoint point){ return point+step; });
}
void Block::changePoints(QVector<QPoint> newPoints){
    blockPoints.swap(newPoints);
}
QPoint Block::rotatePoint(QPoint point, QPoint center){
    return QPoint(point.y()-center.y()+center.x(),std::abs(point.x()-center.x()-center.y()));
}
void Block::rotate90(){
    QPoint rotationCenter(blockPoints[1]);
    std::transform(blockPoints.begin(),blockPoints.end(),blockPoints.begin(), [rotationCenter,this](QPoint point){
        return this->rotatePoint(point,rotationCenter); });
}
QColor Block::getBlockColor(BlockType blockType){
    if(blockType == BlockType::Long){
        return QColor(0, 0, 255);
    }else if(blockType == BlockType::Square){
        return QColor((QRgb)0x00add8e6);
    }else if(blockType == BlockType::Piramid) {
        return QColor(255,165,0);
    }else if(blockType == BlockType::Lefty){
        return QColor(0, 255, 0);
    }else if (blockType == BlockType::Righty) {
        return QColor(138,43,226);
    }else if (blockType == BlockType::Ltype) {
        return QColor(255, 10, 0);
    }else if(blockType == BlockType::InvLtype){
        return QColor(0, 100, 0);
    }else{
        return QColor(255,255,255);
    }
}
BlockType Block::getRandomBlock(){
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> uni_distr(0,6);
    switch(uni_distr(generator)){
        case 0:
            return BlockType::Long;
        case 1:
            return BlockType::Square;
        case 2:
            return BlockType::Piramid;
        case 3:
            return BlockType::Lefty;
        case 4:
            return BlockType::Righty;
        case 5:
            return BlockType::Ltype;
        case 6:
            return BlockType::InvLtype;
        default:
            return BlockType::None;
    }
}
