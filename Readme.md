# Tetris
Tetris game in Qt5.
## Controls:
- **arrow right** - move block right,
- **arrow left** - move block left,
- **arrow up** - rotate block,
- **arrow down** - faster falling,
- **p** - pause game,
- **esc** - exit game
