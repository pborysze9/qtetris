#ifndef BOARD_H
#define BOARD_H

#include <QVector>
#include <QPoint>
#include <algorithm>
#include <memory>
#include <set>
#include "block.h"
#include "direction.h"

class Board{
    private:
        QVector<BlockType> board;
        const QPoint startPoint;
        BlockType currentBlockType;
        int rows;
        int cols;
        std::unique_ptr<Block> blockPtr;
        inline int index(int i,int j);
        void mapBlockToBoard(const QVector<QPoint>& currentBlockCoords);
        void cleanBlocks(const QVector<QPoint>& coords);
        QPoint rotatePoint(QPoint point, QPoint center);
        bool checkIsRowFill(int rowNumber);
        void removeRows(QVector<int> rowsToRemove);
        bool isBlockPointsValid(const QVector<QPoint>& currentBlockCoords);
    public:
        Board(int rows,int cols);
        ~Board();
        bool addNewBlock(BlockType blockType);
        const QVector<BlockType> getBoard() { return board; }
        bool detectBlockCollision(Direction direction);
        void moveBlock(Direction direction);
        void rotateBlock90();
        int checkBoard();
        bool detectBlockCollision(const QVector<QPoint>& coords);

};

#endif // BOARD_H
