#ifndef TETRIS_H
#define TETRIS_H

#include <QWidget>
#include <array>
#include <QPainter>
#include <QKeyEvent>
#include "block.h"
#include "board.h"

class Tetris : public QWidget
{
    Q_OBJECT

public:
    Tetris(QWidget *parent = nullptr);
    ~Tetris();
    static const int board_start_x = 20;
    static const int board_start_y = 20;
    static const int board_width = 300;
    static const int board_height = 600;
    static constexpr int block_size = board_width/10;
    static constexpr int board_width_in_blocks = board_width/block_size;
    static constexpr int board_height_in_blocks = board_height/block_size;
protected:
    void paintEvent(QPaintEvent* event);
    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent* event);
    void drawBoard(QPainter* painter);
private:
    Board* board;
    bool isGameOver;
    bool isPaused;
    long int score;
    int hitBottom;
    int repaintTimer;
    int moveDownTimer;
};
#endif // TETRIS_H
