#ifndef DIRECTION_H
#define DIRECTION_H

enum class Direction{
    Down,
    Left,
    Right
};

#endif // DIRECTION_H
