#ifndef BLOCK_H
#define BLOCK_H

#include <cmath>
#include <QPoint>
#include <QVector>
#include <QColor>
#include <algorithm>
#include <random>
#include "direction.h"

enum class BlockType{
    Long,
    Square,
    Piramid,
    Lefty,
    Righty,
    Ltype,
    InvLtype,
    None
};

class Block{
    private:
        QVector<QPoint> blockPoints;
        BlockType blockType;
        QPoint rotationCenter;
        QPoint rotatePoint(QPoint point, QPoint center);
    public:
        Block(BlockType blockType,QPoint startPoint);
        BlockType getBlockType() { return blockType; }
        void rotate90();
        void changePoints(QVector<QPoint> newPoints);
        void move(Direction direction);
        const QVector<QPoint>& getPoints();
        static QColor getBlockColor(BlockType blockType);
        static BlockType getRandomBlock();
};


#endif // BLOCK_H
